<h1 align="center">Aprendendo OOP</h1>

## Programação Procedural
Antes da programação Orientada a Objeto, houve a programação procedural, que dividia o programa em funções. Então tinhamos os dados guardados em várias variáveis e funções para mexer com os dados.
```js
let var1 = 1
let var2 = 2
let var3 = 3

function matematico(var1,var2,var3) {
  let frifairi
  return frifairi = (var3 + var1 * var2);
}
```
## Programação Orientada a Objetos
Na programação orientada a objetos, nós pegamos funções e dados relacionados e colocamos em objetos.<br>
Funções → passam a se chamar Métodos.<br>
Dados → passam a ser propriedades.

Rust é Orientado a objetos: Structs e Enums tem dados, e blocos `impl` fornecem métodos em structs e enums.

Por exemplo, um carro, um carro é um objeto com propriedades como:
 - Marca
 - Modelo
 - Cor
 
E métodos como:
 - Acelerar
 - Frear
 - Businar
 
## Encapsulamento
Na programação orientada a objetos nós agrupamos variáveis e funções relacionadas que operam neles em objetos, e isso é o que chamamos de encapsulamento.

> JS
```js
let salarioBase = 30_000
let horasExtras = 10
let rate = 20

function obterSalario(salariobase, horasExtras, rate) {
  return salariobase + (horasExtras * rate)
}
```

> Rust
```rust
const SALARIOBASE: u32 = 30000;
const HORASEXTRAS: u32 = 10;
const RATE: u32 = 20;

fn obter_salario() -> u32 {
    return SALARIOBASE + (HORASEXTRAS * RATE);
}

fn main() {
    println!("{:?}", obter_salario());
}
```
Na programação procedural é assim, temos variáveis em um canto e funções em outro, eles são desassociados.

Agora, veremos na programação orientada a objetos:
> JS
```js
let empregado = {
  salarioBase: 30000
  horasExtras: 10
  rate: 20
  obterSalario: function() {
    return this.salariobase + (this.horasExtras * this.rate);
  }
}
```
Por que é melhor?
Nessa implementação não precisamos de parametros nem coisa assim, porque os parametros já existem, são propriedades do objeto. Tudo está relacionado, então são partes de um objeto. 
Na programação procedural, as vezes precisamos de muitos e muitos parametros

> "The best functions are those with no parameters!" - Uncle Bob

## Abstração
Pense em um DVD Player, ele tem um circuito complexo por dentro, e alguns botões por fora, você não se importa com o que tem dentro e como funciona, está tudo escondido de você. Isso é abstração. Podemos usar a mesma técnica em objetos na OOP, podemos esconder alguns métodos e propriedades do lado de fora, com isso, temos alguns benefícios:
 - Inteface mais simples
entender um objeto com poucas propriedades e métodos é mais fácil do que um objeto com várias propriedades e métodos
 - Reduzir o impacto das mudanças

Vamos fingir que amanhã a gente resolva mudar esses métodos privados ou internos, essas mudanças vão vazar pra fora, porque não temos código para mexer com esses métodos por fora do seu objeto. Se quisermos apagar um método ou mudar seus parametros, nenhuma dessas mudanças vão impactar o resto do código. 

## Herança
A herança permite que você elimine código redundante<br>
`[Continuar...]`

<!--- 
## Polimorfismo
##  RDBC --->
